import portalocker

def value_following(current_value):
    return current_value + 1

class PersistentCounter(object):
    """
    A monotonically incrementing counter that stores its value in a file in
    the filesystem and uses file locking so that it can be safely used to
    synchronise operations between independent processes.

    Function name conventions:

    In general, methods including "get_next" in their name may modify the
    counter, whereas methods with "peek" in their name may read the file but
    will not modify the counter.

    That may seem contrary to conventional unmodifying "getter" naming, however
    in the context of atomic operations on a counter seem more in tune with
    typical human language.
    """

    def __init__(self, filepath):
        self.filepath = filepath

        # Ensures counter file exists without writing any default value
        with portalocker.Lock(self.filepath, mode='a') as f:
            pass

    @classmethod
    def _read_from_file(cls, f):
        # Prepending 0: initial (empty) file reads as value zero
        # Everything else is numerically unchanged
        return int('0' + f.read())

    @classmethod
    def _write_to_file(cls, f, value):
        f.seek(0)
        f.truncate()
        f.write(str(value))

    def get_next(self):
        """Atomically increment the counter and return the new value."""
        with portalocker.Lock(self.filepath, mode='r+') as f:
            current_value = self._read_from_file(f)
            next_value = value_following(current_value)
            self._write_to_file(f, next_value)
        return next_value

    def get_next_if_lt(self, less_than):
        """
        Atomically read the current value of the counter, then increment and
        return a new value only if the specified condition is met by the
        current value.

        If the current value is less than ``less_than``, the new value is
        calculated and returned.
        If the current value is not less than ``less_than``, None is returned.
        """
        next_value = None
        with portalocker.Lock(self.filepath, mode='r+') as f:
            # Prepending 0: default (empty) file reads as value zero
            # Everything else is numerically unchanged
            current_value = self._read_from_file(f)
            if current_value < less_than:
                next_value = value_following(current_value)
                self._write_to_file(f, next_value)
        return next_value

    # TODO consider whether it is reasonable to return 0
    # by implication... is it reasonable to have this function at all?
    def peek_current(self):
        """Read the current value of the counter without modifying it."""
        with portalocker.Lock(self.filepath, mode='r') as f:
            current_value = self._read_from_file(f)
        return current_value

    def peek_next(self):
        return value_following(self.peek_current())


# These two classes are very similar in behaviour and implementation,
# however I decided that to make them share code would abstract them
# too much. Leaving them as-is.

class RangeIterator:
    """
    RangeIterator can be used for iterating over the range of values
    # between two counters. Specifically, values returned will be in the range:
    ( start, end ]   (taking the value to be that of peek_current())
    Caveat from below applies:

    Counters are not locked and the start counter may be updated before
    calling code can use a value obtained from the iterator. If the
    iterator finds that either counter has been updated, it will update
    itself to use the new value.

    From this flow two implications:
    There may be gaps in iteration, e.g.: 3 4 5 8 9

    After a for loop on an iterator has exited, looping again on the
    same iterator may yield further values. This latter behaviour may be
    helpful for setting up a 'watch' on range between a pair of counters.
    e.g.

    cri = RangeIterator:
    while True:
        for v in cri:
            # do something with v ...
        time.sleep(60)
    """
    def __init__(self, range_start_counter, range_end_counter):
        self.range_start_counter = range_start_counter
        self.range_end_counter = range_end_counter
        self.next_candidate = self.range_start_counter.peek_next()
        self.current_range_end = self.range_end_counter.peek_current()

    def __iter__(self):
        return self

    def __next__(self):
        if self.next_candidate > self.current_range_end:
            self.current_range_end = self.range_end_counter.peek_current()
            if self.next_candidate > self.current_range_end:
                raise StopIteration
        current_range_start = self.range_start_counter.peek_next()
        if self.next_candidate < current_range_start:
            self.next_candidate = current_range_start
        n = self.next_candidate
        self.next_candidate = value_following(self.next_candidate)
        return n

class Follower:
    """
    Follower is similar to the RangeIterator in that is is an
    iterator, is initialised with a pair of counters, and returns
    values from the range between the two iterators.

    However, whereas a RangeIterator will not modify either of the
    underlying counters, a Follower obtains values by incrementing the
    'follower' counter.

    The guarantee is that a Follower will never obtain a value from
    the follower that hasn't previously been obtained from the leader.

    This can be used to implement producer / consumer behaviour, while
    the RangeIterator can be used to iterate over the
    produced-but-not-yet-consumed range.
    """
    def __init__(self, following_counter, leading_counter):
        self.follower = following_counter
        self.leader = leading_counter
        self.leader_value = self.leader.peek_current()

    def __iter__(self):
        return self

    def __next__(self):
        n = self.get_next_if_available()
        if n is None:
            raise StopIteration
        return n

    def get_next_if_available(self):
        n = self.follower.get_next_if_lt(self.leader_value)
        if n is None:
            leader_value = self.leader.peek_current()
            n = self.follower.get_next_if_lt(self.leader_value)
        return n
