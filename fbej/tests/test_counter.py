"""Sample unit test module using pytest-describe and expecter."""
# pylint: disable=redefined-outer-name,unused-variable,expression-not-assigned,singleton-comparison

import multiprocessing
import os
import pytest

from expecter import expect
from fbej import counter
from pytest_describe import behaves_like


def initialised_counter(path, initial_file_content : str):
    with open(path, 'w') as f:
        f.write(initial_file_content)
    return counter.PersistentCounter(path)

def describe_counter():

    def describe_a_new_counter():

        @pytest.fixture
        def new_counter(tmpdir):
            return counter.PersistentCounter(os.path.join(str(tmpdir), "testfile"))

        def has_a_current_value_of_zero(new_counter):
            expect(new_counter.peek_current()) == 0

        def begins_counting_at_one(new_counter):
            expect(new_counter.get_next()) == 1

    # If we imagine a use case with 1000 increments per second,
    # and that the system enjoys constant use for 15 years, then
    # the number of increments that need be supported is approximately:
    #
    # 15 * 365 * 24 * 60 * 60 * 1000 = 473,040,000,000

    def describe_any_counter():

        @pytest.fixture(params = ["13", "42", "1600", "480000000000"])
        def a_counter(request, tmpdir):
            return initialised_counter(os.path.join(str(tmpdir), "testfile"),
                                       request.param)

        def returns_an_integer_value(a_counter):
            expect(a_counter.peek_current()).isinstance(int)

        def returns_a_different_value_for_each_call_to_get_next(a_counter):
            first_value = a_counter.get_next()
            next_value = a_counter.get_next()
            expect(next_value) != first_value

        def is_incremented_monotonically(a_counter):
            initial_value = a_counter.peek_current()
            expect(a_counter.get_next()) == initial_value + 1

    @pytest.mark.slow
    def can_safely_be_used_from_multiple_processes(tmpdir):

        def f(instance, fp, count_to):
            c = counter.PersistentCounter(fp)
            for i in range(count_to):
                c.get_next()

        # Note that because this test is simply trying to provoke a race condition, it may generate false passes
        # but it shouldn't create false fails
        PROCESSES = 5
        INCREMENTS = 1000

        fp = os.path.join(str(tmpdir), "testfile")
        ps = []
        for i in range(PROCESSES):
            p = multiprocessing.Process(target=f, args=(i,fp,INCREMENTS))
            ps.append(p)
        for p in ps:
            p.start()
        for p in ps:
            p.join()
        
        expect(counter.PersistentCounter(fp).peek_current()) == PROCESSES * INCREMENTS

    @pytest.mark.slow
    def multiple_process_test_is_good(tmpdir):

        def f(instance, fp, count_to):
            c = counter.PersistentCounter(fp)
            for i in range(count_to):
                c.get_next()

        # Additional thread updates counter without locking file
        # Should demonstrate that the previous test would fail if updates weren't atomic
        # So making sure the counter isn't relying on behaviour it shouldn't
        def g(instance, fp, count_to):
            for i in range(count_to):
                with open(fp, 'r+') as f:
                    v = PersistentCounter._read_from_file(f)
                    v = counter.value_following(v)
                    PersistentCounter._write_to_file(f, v)

        PROCESSES = 5
        INCREMENTS = 1000

        fp = os.path.join(str(tmpdir), "testfile")
        ps = []
        for i in range(PROCESSES):
            if i == 0:
                target=g
            else:
                target=f
            p = multiprocessing.Process(target=target, args=(i,fp,INCREMENTS))
            ps.append(p)
        for p in ps:
            p.start()
        for p in ps:
            p.join()

        expect(counter.PersistentCounter(fp).peek_current()) != PROCESSES * INCREMENTS

def describe_follower():

    def describe_a_follower_that_has_value_less_than_its_leader():

        @pytest.fixture(
            params = [("0", "1"),
                      ("42", "52"),
                      ("9997", "9999"),
                      ("0", "480000000000")]
        )
        def a_follower(request, tmpdir):
            fval,lval = request.param
            follower = initialised_counter(os.path.join(str(tmpdir), "follower"), fval)
            leader = initialised_counter(os.path.join(str(tmpdir), "leader"), lval)
            return counter.Follower(follower, leader)

        def will_return_an_integer(a_follower):
            next_value = a_follower.get_next_if_available()
            expect(next_value).isinstance(int)

    def describe_a_follower_that_has_value_equal_to_its_leader():

        @pytest.fixture(
            params = [("0", "0"),
                      ("1", "1"),
                      ("392", "392"),
                      ("999678", "999678")]
        )
        def a_follower(request, tmpdir):
            fval,lval = request.param
            follower = initialised_counter(os.path.join(str(tmpdir), "follower"), fval)
            leader = initialised_counter(os.path.join(str(tmpdir), "leader"), lval)
            return counter.Follower(follower, leader)

        def will_return_none(a_follower):
            next_value = a_follower.get_next_if_available()
            expect(next_value) == None

def describe_range_iterator():

    def describe_a_range_iterator_that_has_a_start_value_less_than_the_end_value():

        @pytest.fixture(
            params = [("0", "2"),
                      ("0", "10"),
                      ("9997", "9999"),
                      ("480000000000", "480000000020")]
        )
        def a_rangeiterator(request, tmpdir):
            fval,lval = request.param
            start = initialised_counter(os.path.join(str(tmpdir), "start"), fval)
            end = initialised_counter(os.path.join(str(tmpdir), "end"), lval)
            return counter.RangeIterator(start, end)

        def will_iterate_over_a_range_of_integers(a_rangeiterator):
            for i in a_rangeiterator:
                expect(i).isinstance(int)

        def will_begin_iteration_at_the_start_counters_next_value(a_rangeiterator):
            v = None
            for i in a_rangeiterator:
                if v is None:
                    v = i
            expect(v) == a_rangeiterator.range_start_counter.peek_next()

        def will_end_iteration_at_the_end_counters_current_value(a_rangeiterator):
            for i in a_rangeiterator:
                v = i
            expect(v) == a_rangeiterator.range_end_counter.peek_current()

    def describe_a_range_iterator_that_has_a_start_value_equal_to_the_end_value():

        @pytest.fixture(
            params = [("0", "0"),
                      ("1", "1"),
                      ("392", "392"),
                      ("999678", "999678")]
        )
        def a_rangeiterator(request, tmpdir):
            fval,lval = request.param
            start = initialised_counter(os.path.join(str(tmpdir), "start"), fval)
            end = initialised_counter(os.path.join(str(tmpdir), "end"), lval)
            return counter.RangeIterator(start, end)

        def will_not_return_any_values_when_iterated_with(a_rangeiterator):
            iterated = False
            for i in a_rangeiterator:
                iterated = True
            expect(iterated) == False

    def describe_a_range_iterator_that_has_been_iterated_until_it_stopped():

        @pytest.fixture(
            params = [("0", "2"),
                      ("0", "10"),
                      ("9997", "9999"),
                      ("480000000000", "480000000020")]
        )
        def a_rangeiterator(request, tmpdir):
            fval,lval = request.param
            start = initialised_counter(os.path.join(str(tmpdir), "start"), fval)
            end = initialised_counter(os.path.join(str(tmpdir), "end"), lval)
            ri = counter.RangeIterator(start, end)
            for i in ri:
                pass
            return ri

        def will_not_return_any_values_when_iterated_a_second_time(a_rangeiterator):
            iterated = False
            for i in a_rangeiterator:
                iterated = True
            expect(iterated) == False

        def will_return_the_next_highest_value_when_iterated_after_the_next_value_has_been_got_from_the_end_counter(a_rangeiterator):
            next_end = a_rangeiterator.range_end_counter.get_next()
            vs = None
            ve = None
            for i in a_rangeiterator:
                if vs is None:
                    vs = i
                ve = i
            expect(vs) == next_end
            expect(ve) == next_end

    @pytest.fixture
    def a_rangeiterator_1_to_10(tmpdir):
        start = initialised_counter(os.path.join(str(tmpdir), "start"), "0")
        end = initialised_counter(os.path.join(str(tmpdir), "end"), "10")
        ri = counter.RangeIterator(start, end)
        return ri

    def if_the_start_counter_overtakes_the_iterator_the_iterator_will_skip_to_the_start_counter_value(a_rangeiterator_1_to_10):
        ri = a_rangeiterator_1_to_10
        seen_values = []
        for i in ri:
            seen_values.append(i)
            if i == 4:
                for n in range(6):
                    ri.range_start_counter.get_next()
        expect(seen_values) == [1,2,3,4,7,8,9,10]

    def if_the_end_counter_is_advanced_during_the_iterators_lifetime_the_iterator_will_carry_on_to_the_new_end_value(a_rangeiterator_1_to_10):
        ri = a_rangeiterator_1_to_10
        seen_values = []
        for i in ri:
            seen_values.append(i)
            if i == 4:
                for n in range(3):
                    ri.range_end_counter.get_next()
        expect(seen_values) == [1,2,3,4,5,6,7,8,9,10,11,12,13]
