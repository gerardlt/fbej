FBEJ
====

A simple, inter-process event queue based on file storage. It doesn't
require any server or daemon.

It is probably not suitable for large-scale use, but those use cases
are well served by existing tools. The intended use case for this
project is small (single-machine) systems comprised of sets of
programs (e.g. cli commands + cronjobs).

## Installation

Install fbej directly from the source code:

```sh
$ git clone https://github.com/gerardlt/fbej.git
$ cd ptq
$ python setup.py install
```

## Usage

After installation, the package can imported:

```sh
$ python
>>> import fbej
>>> fbej.__version__
```

TODO Write up some examples of use...
